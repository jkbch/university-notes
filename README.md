# University notes

University notes written with Emacs Orgmode. The notes are written in Danish.

The repository contains note for the following Aarhus University courses
*  Calculus beta (calculus.org)
*  Algoritmer og datastrukturer (algoritmer.org)